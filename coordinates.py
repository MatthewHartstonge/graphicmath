""" Coordinates for Graphical computation

Program Name: coordinates.py
Date Written: 01/05/2015
Written By: Matt Hartstonge

"""

# Library imports

# Our Library imports

# Constants
_VECTOR = 0
_POINT = 1


class Point(object):
    _x = None
    _y = None
    _z = None

    @property
    def x(self):
        """ Returns the X coordinate of the point
        """
        if self._x is None:
            return None
        else:
            return float(self._x)

    @property
    def y(self):
        """ Returns the Y coordinate of the point
        """
        if self._y is None:
            return None
        else:
            return float(self._y)

    @property
    def z(self):
        """ Returns the Z coordinate of the point
        """
        if self._z is None:
            return None
        else:
            return float(self._z)

    # Class functions
    def is_valid_2d(self):
        """ Checks if the point is valid as a 2D coordinate

        Returns:
            Bool: Depending on if a valid coordinate
        """

        if (self.x or self.x == 0) and \
           (self.y or self.y == 0):
            return True
        else:
            return False

    def is_valid_3d(self):
        """ Checks if the point is valid as a 3D coordinate

        Returns:
            Bool: Depending on if a valid coordinate
        """

        if (self.x or self.x == 0) and \
           (self.y or self.y == 0) and \
           (self.z or self.z == 0):
            return True
        else:
            return False


# Vector helper functions that don't need to be tied to the class
def _init_helper_validate_point_objects(point1=None, point2=None):
    """ Private function for helping validate point objects for a vector.

    Args:
        point1 (Point): A Point Object
        point2 (Point): A Point Object

    Returns:
        True: If objects are able to be validated

    Raises:
        ValueError: If the first point can't be validated
        ValueError: If the second point can't be validated
        IOError: If no Point objects were passed in

    """
    if point1 and point2:
        if point1.is_valid_3d:
            if point2.is_valid_3d:
                return True
            else:
                ValueError("Please enter a second valid point!")
        else:
            raise ValueError("Please enter a valid first point!")
    else:
        IOError("No Point objects provided!")


def __init_helper_validate_points(x1, y1, z1, x2, y2, z2):
    """ Validates passed in points and creates point classes as needed

    Args:
        x1 (float): The X Co-Ordinate for the first point
        y1 (float): The Y Co-Ordinate for the first point
        z1 (float): The Z Co-Ordinate for the first point
        x2 (float): The X Co-Ordinate for the second point
        y2 (float): The Y Co-Ordinate for the second point
        z2 (float): The Z Co-Ordinate for the second point

    Returns:

    Raises:

    """
    pass


class Vector(object):
    """ Enables the creation of a Vector through taking in Point Classes

    Attributes:
        _point1 (Point):
            A private variable that holds a Point class
        _point2 = None
            A private variable that holds a Point class
        _vx = None
            The Vector X value
        _vy = None
            The Vector Y value
        _vz = None
            The Vector Z value

    """

    _point1 = None
    _point2 = None
    _vx = None
    _vy = None
    _vz = None

    def __init__(self,
                 x1=None, y1=None, z1=None,
                 x2=None, y2=None, z3=None,
                 point1=None,
                 point2=None):
        """ Constructor for a Vector

        Args:
            point_1 (Point):
                A reference to a Point Class that makes up the Vector
            point_2 (Point):
                The second Point class that makes up the Vector

        """
        # Accept points over sets
        if point1 and point2:
            if _init_helper_validate_point_objects(point1, point2):
                pass
        else:
            pass



class HomogeneousCoordinateFloat(object):
    """ Defines a Homogeneous Co-ordinate with floating point numbers

    Attributes:
        _x (float): Private variable storing an the x value of a coordinate
        _y (float): Private variable storing an the y value of a coordinate
        _z (float): Private variable storing an the z value of a coordinate
        _w (int): Private variable storing whether the variable is a point or vector as defined by OpenGL

    """
    _x = None
    _y = None
    _z = None
    _w = 0

    def __init__(self,
                 x_input=None,
                 y_input=None,
                 z_input=None,
                 w_input=None,
                 point=None,
                 vector=None,
                 ):
        """ Enables the creation of a Homogeneous coordinate

        Args:
            x_input (float):
                The value of X as a floating point or an integer
            y_input (float):
                The value of X as a floating point or an integer
            z_input (float):
                The value of X as a floating point or an integer
            w_input (int):
                Defines whether the co-ordinates are a point or a vector
            point (Point):
                Takes in and processes a Point class (preferred over vectors)
            vector (Vector):
                Takes in and processes a Vector class
        """
        if point or vector:
            self.x = point.x or vector.x
            self.y = point.y or vector.y
            self.z = point.z or vector.z
            if point:
                self.w = _POINT
            else:
                self.w = _VECTOR

        else:
            self.x = x_input
            self.y = y_input
            self.z = z_input
            self.w = w_input

    @property
    def x(self):
        """ Returns X coord as a floating point number
        """
        return float(self._x)

    @x.setter
    def x(self, x_value):
        """ Sets X coord explicitly casting it as a floating point number
        Args:
            x_value (float): The FP number to be set as the X value
        """
        if x_value:
            self._x = float(self._x)
        else:
            raise ValueError("Please enter an X Coordinate!")

    @property
    def y(self):
        """ Returns y coord as a floating point number
        """
        return float(self._y)

    @y.setter
    def y(self, y_value):
        """ Sets Y coord explicitly casting it as a floating point number
        Args:
            y_value (float): The FP number to be set as the Y value
        """
        if y_value:
            self._y = float(self._y)
        else:
            raise ValueError("Please enter an Y Coordinate!")

    @property
    def z(self):
        """ Returns z coord as a floating point number
        """
        return float(self._z)

    @z.setter
    def z(self, z_value):
        """ Sets Z coord explicitly casting it as a floating point number
        Args:
            z_value (float): The FP number to be set as the Z value
        """
        if z_value:
            self._z = float(self._z)
        else:
            raise ValueError("Please enter an Z Coordinate!")

    @property
    def w(self):
        """ Returns whether the coordinate is a point or vector
        """
        return int(self._w)

    @w.setter
    def w(self, w_value):
        """ Sets W explicitly casting it as a integer.
        Defines whether the coordinate is a point or a vector

        Args:
            w_value (int): The int to specify if it is a _VECTOR or _POINT
        """
        if w_value:
            if w_value in (_VECTOR, _POINT):
                self._w = float(self._w)
            else:
                raise ValueError("Please specify whether the coordinate is _VECTOR or _POINT")
        else:
            raise ValueError("Please enter a value for W!")

    def normalise_vector(self):
        """ Returns a normalised

        :return:
        """

a_point = Point()
a_point._x = 12
print a_point.x
print a_point.is_valid_2d()
print a_point.is_valid_3d()

a_point._y = 0
print a_point.x
print a_point.y
print a_point.is_valid_2d()
print a_point.is_valid_3d()

a_point._z = -3
print a_point.x
print a_point.y
print a_point.z
print a_point.is_valid_2d()
print a_point.is_valid_3d()